const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = {
  module: {
    rules: [
      {
          test: /\.vue$/,
          exclude: /node_modules/,
          loader: 'vue-loader'
      },
      {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
          loader: 'babel-loader'
          }
      },
      {
        test: /\.scss$/,
        use: [
            "vue-style-loader", // loads from style tags in components
            "css-loader", // translates CSS into CommonJS
            {
              loader: "sass-loader" // compiles Sass to CSS, using Node Sass by default
            }
        ]
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new VueLoaderPlugin(),
    new DashboardPlugin()
  ]
}
